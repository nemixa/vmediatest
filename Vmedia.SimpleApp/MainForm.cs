﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Vmedia.SimpleApp
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();

            tbCredit.Text = "346129457012763";
            tbPhone.Text = "6 51)  55 54 21  3";
            tbNumber.Text = "123456789.123";
        }

        private Regex _creditRegex = new Regex(@"
^(?:(?<visa>4[0-9]{12}(?:[0-9]{3})?)          # Visa
 |  (?<mc>(?:5[1-5][0-9]{2}                # MasterCard
     | 222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720)[0-9]{12})
 |  (?<amex>3[47][0-9]{13})                   # American Express
 |  (?<dc>3(?:0[0-5]|[68][0-9])[0-9]{11})   # Diners Club
 |  (?<disc>6(?:011|5[0-9]{2})[0-9]{12})      # Discover
 |  (?<jcb>(?:2131|1800|35\d{3})\d{11})      # JCB
)$", RegexOptions.IgnorePatternWhitespace);

        private static Dictionary<string, string> _cardsTitles = new Dictionary<string, string>
        {
            {"visa", "Visa"},
            {"mc", "MasterCard"},
            {"amex", "American Express"},
            {"dc", "Diners Club"},
            {"disc", "Discover"},
            {"jcb", "JCB"},
        };
        private void tbCredit_TextChanged(object sender, EventArgs e)
        {
            
            var m = _creditRegex.Match(tbCredit.Text);
            if (m.Success)
            {
                foreach (var pair in _cardsTitles)
                {
                    if (m.Groups[pair.Key].Success)
                    {
                        lCredit.Text = pair.Value;
                        return;
                    }
                }
            }
            else
            {
                lCredit.Text = "Unknown";
            }
        }

        private Regex _phoneRegex = new Regex(@"\(?\s*(\d)\s*(\d)\s*(\d)\s*\)?\s*(\d)\s*(\d)\s*(\d)\s*(\d)\s*(\d)\s*(\d)\s*(\d)");
        private void tbPhone_TextChanged(object sender, EventArgs e)
        {
            string r = _phoneRegex.Replace(tbPhone.Text, "($1$2$3) $4$5$6-$7$8$9$10");
            lPhone.Text = r;
        }

        private Regex _numberRegex = new Regex(@"(\d)(?=(?:\d{3})+(?!\d)(?=\.))");
        private void tbNumber_TextChanged(object sender, EventArgs e)
        {
            string r = _numberRegex.Replace(tbNumber.Text, "$1,");
            lNumber.Text = r;
        }
    }
}

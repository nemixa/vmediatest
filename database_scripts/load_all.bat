@for /F "delims=" %%a in (database.txt) do (set db=%%a)
@SET outDir=for_load/
bcp %db%.dbo.TransactionType IN "%outDir%TransactionType.txt" -c -T -E
bcp %db%.dbo.Currencies IN "%outDir%Currencies.txt" -c -T -E
bcp %db%.dbo.Clients IN "%outDir%Clients.txt" -c -T -E
bcp %db%.dbo.ClientBalances IN "%outDir%ClientBalances.txt" -c -T -E
bcp %db%.dbo.Transactions IN "%outDir%Transactions.txt" -c -T -E
bcp %db%.dbo.TransactionsArchive IN "%outDir%TransactionsArchive.txt" -c -T -E
pause
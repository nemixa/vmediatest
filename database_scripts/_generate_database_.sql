USE [master]
GO

DROP DATABASE [vmedia]
GO

CREATE DATABASE [vmedia]
GO

USE [vmedia]
GO
/****** Object:  UserDefinedTableType [dbo].[OperationParams]    Script Date: 09.01.2017 19:59:02 ******/
CREATE TYPE [dbo].[OperationParams] AS TABLE(
	[SourceBalanceId] [int] NOT NULL,
	[SourceValue] [decimal](18, 2) NOT NULL,
	[OperationId] [int] NOT NULL,
	[TargetBalanceId] [int] NULL
)
GO
/****** Object:  StoredProcedure [dbo].[AddTransaction]    Script Date: 09.01.2017 19:59:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[AddTransaction](
	@params AS [OperationParams] readonly
) AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION op
	BEGIN TRY
		INSERT INTO dbo.Transactions (SourceBalanceId, TargetBalanceId, OperationId, SourceValue, [Date])
		SELECT SourceBalanceId, TargetBalanceId, OperationId, SourceValue, GETDATE() FROM @params

		MERGE dbo.ClientBalances AS tgt
		USING (
			SELECT q.id AS id, SUM(q.v) AS val FROM (
				SELECT SourceBalanceId AS id, -SUM(SourceValue) AS v
				FROM @params
				WHERE OperationId = 1 OR OperationId = 3
				GROUP BY SourceBalanceId
				UNION ALL
				SELECT SourceBalanceId AS id, SUM(SourceValue) AS v
				FROM @params
				WHERE OperationId = 2
				GROUP BY SourceBalanceId
				UNION ALL
				SELECT TargetBalanceId AS id, SUM(SourceValue) AS v
				FROM @params
				WHERE OperationId = 3
				GROUP BY TargetBalanceId
			) AS q 
			GROUP BY q.id
		) AS src
		ON tgt.Id = src.id
		WHEN MATCHED THEN
			UPDATE SET tgt.Amount = tgt.Amount + src.val;
		
		COMMIT TRANSACTION op
		SELECT '0'
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION op
		SELECT ERROR_MESSAGE()
	END CATCH	
END
GO
/****** Object:  StoredProcedure [dbo].[BackupTransactions]    Script Date: 09.01.2017 19:59:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[BackupTransactions]
AS
BEGIN
	DELETE FROM dbo.Transactions
	OUTPUT 
		DELETED.Id,
		DELETED.SourceBalanceId, DELETED.TargetBalanceId, 
		DELETED.OperationId, DELETED.SourceValue, DELETED.[Date], 
		GETDATE() AS ArchiveDate
		INTO [dbo].[TransactionsArchive] 
END

GO
/****** Object:  Table [dbo].[ClientBalances]    Script Date: 09.01.2017 19:59:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ClientBalances](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ClientId] [int] NOT NULL,
	[Currency] [nchar](3) NOT NULL,
	[Amount] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_ClientBalances] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Clients]    Script Date: 09.01.2017 19:59:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Clients](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Login] [nvarchar](50) NOT NULL,
	[Email] [nvarchar](254) NOT NULL,
	[Password] [nvarchar](max) NOT NULL,
	[RegDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Clients] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Currencies]    Script Date: 09.01.2017 19:59:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Currencies](
	[Code] [nchar](3) NOT NULL,
	[Name] [nvarchar](50) NULL,
 CONSTRAINT [PK_Currencies] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Transactions]    Script Date: 09.01.2017 19:59:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Transactions](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[SourceBalanceId] [int] NOT NULL,
	[TargetBalanceId] [int] NULL,
	[OperationId] [int] NOT NULL,
	[SourceValue] [decimal](18, 2) NULL,
	[Date] [datetime] NOT NULL,
 CONSTRAINT [PK_Transactions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TransactionsArchive]    Script Date: 09.01.2017 19:59:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TransactionsArchive](
	[Id] [bigint] NOT NULL,
	[SourceBalanceId] [int] NOT NULL,
	[TargetBalanceId] [int] NULL,
	[OperationId] [int] NOT NULL,
	[SourceValue] [decimal](18, 2) NULL,
	[Date] [datetime] NOT NULL,
	[ArchiveDate] [datetime] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TransactionType]    Script Date: 09.01.2017 19:59:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TransactionType](
	[Id] [int] NOT NULL,
	[Operation] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_TransactionType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[AllTransactions]    Script Date: 09.01.2017 19:59:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[AllTransactions]
AS
SELECT Id, SourceBalanceId, TargetBalanceId, OperationId, SourceValue, [Date]
FROM dbo.Transactions
UNION ALL
(SELECT Id, SourceBalanceId, TargetBalanceId, OperationId, SourceValue, [Date]
FROM dbo.TransactionsArchive)

GO
SET IDENTITY_INSERT [dbo].[ClientBalances] ON 

GO
INSERT [dbo].[ClientBalances] ([Id], [ClientId], [Currency], [Amount]) VALUES (1, 2, N'USD', CAST(0 AS Decimal(18, 2)))
GO
INSERT [dbo].[ClientBalances] ([Id], [ClientId], [Currency], [Amount]) VALUES (2, 2, N'CAD', CAST(0 AS Decimal(18, 2)))
GO
INSERT [dbo].[ClientBalances] ([Id], [ClientId], [Currency], [Amount]) VALUES (3, 2, N'EUR', CAST(0 AS Decimal(18, 2)))
GO
INSERT [dbo].[ClientBalances] ([Id], [ClientId], [Currency], [Amount]) VALUES (4, 4, N'USD', CAST(0 AS Decimal(18, 2)))
GO
INSERT [dbo].[ClientBalances] ([Id], [ClientId], [Currency], [Amount]) VALUES (5, 4, N'CAD', CAST(0 AS Decimal(18, 2)))
GO
INSERT [dbo].[ClientBalances] ([Id], [ClientId], [Currency], [Amount]) VALUES (6, 4, N'EUR', CAST(0 AS Decimal(18, 2)))
GO
SET IDENTITY_INSERT [dbo].[ClientBalances] OFF
GO
SET IDENTITY_INSERT [dbo].[Clients] ON 

GO
INSERT [dbo].[Clients] ([Id], [Login], [Email], [Password], [RegDate]) VALUES (2, N'nmwind', N'nmwind@gmail.com', N'1Qweasd', CAST(0x0000A6F200000000 AS DateTime))
GO
INSERT [dbo].[Clients] ([Id], [Login], [Email], [Password], [RegDate]) VALUES (4, N'isaev', N'isaev@mail.ru', N'1Qweasd', CAST(0x0000A6F300000000 AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[Clients] OFF
GO
INSERT [dbo].[Currencies] ([Code], [Name]) VALUES (N'CAD', NULL)
GO
INSERT [dbo].[Currencies] ([Code], [Name]) VALUES (N'EUR', NULL)
GO
INSERT [dbo].[Currencies] ([Code], [Name]) VALUES (N'USD', NULL)
GO

INSERT [dbo].[TransactionType] ([Id], [Operation]) VALUES (1, N'Withdraw')
GO
INSERT [dbo].[TransactionType] ([Id], [Operation]) VALUES (2, N'Deposit')
GO
INSERT [dbo].[TransactionType] ([Id], [Operation]) VALUES (3, N'Transfer between accounts')
GO


ALTER TABLE [dbo].[ClientBalances]  WITH CHECK ADD  CONSTRAINT [FK_ClientBalances_Clients] FOREIGN KEY([ClientId])
REFERENCES [dbo].[Clients] ([Id])
GO
ALTER TABLE [dbo].[ClientBalances] CHECK CONSTRAINT [FK_ClientBalances_Clients]
GO
ALTER TABLE [dbo].[ClientBalances]  WITH CHECK ADD  CONSTRAINT [FK_ClientBalances_Currencies] FOREIGN KEY([Currency])
REFERENCES [dbo].[Currencies] ([Code])
GO
ALTER TABLE [dbo].[ClientBalances] CHECK CONSTRAINT [FK_ClientBalances_Currencies]
GO
ALTER TABLE [dbo].[Transactions]  WITH CHECK ADD  CONSTRAINT [FK_Transactions_ClientBalances] FOREIGN KEY([SourceBalanceId])
REFERENCES [dbo].[ClientBalances] ([Id])
GO
ALTER TABLE [dbo].[Transactions] CHECK CONSTRAINT [FK_Transactions_ClientBalances]
GO
ALTER TABLE [dbo].[Transactions]  WITH CHECK ADD  CONSTRAINT [FK_Transactions_ClientBalances1] FOREIGN KEY([TargetBalanceId])
REFERENCES [dbo].[ClientBalances] ([Id])
GO
ALTER TABLE [dbo].[Transactions] CHECK CONSTRAINT [FK_Transactions_ClientBalances1]
GO
ALTER TABLE [dbo].[Transactions]  WITH CHECK ADD  CONSTRAINT [FK_Transactions_TransactionType] FOREIGN KEY([OperationId])
REFERENCES [dbo].[TransactionType] ([Id])
GO
ALTER TABLE [dbo].[Transactions] CHECK CONSTRAINT [FK_Transactions_TransactionType]
GO
@for /F "delims=" %%a in (database.txt) do (set db=%%a)
@SET outDir=saves/
bcp %db%.dbo.ClientBalances OUT "%outDir%ClientBalances.txt" -c -T
bcp %db%.dbo.Clients OUT "%outDir%Clients.txt" -c -T
bcp %db%.dbo.Currencies OUT "%outDir%Currencies.txt" -c -T
bcp %db%.dbo.TransactionType OUT "%outDir%TransactionType.txt" -c -T
bcp %db%.dbo.Transactions OUT "%outDir%Transactions.txt" -c -T
bcp %db%.dbo.TransactionsArchive OUT "%outDir%TransactionsArchive.txt" -c -T
pause
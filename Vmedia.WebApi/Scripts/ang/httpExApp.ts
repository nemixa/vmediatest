﻿// ReSharper disable TsResolvedFromInaccessibleModule

namespace Ang {
    export class HttpExService {
        public http: any;
        private authService: AuthSerivce;

        constructor(http:any, authSerivce:AuthSerivce) {
            this.http = http;
            this.authService = authSerivce;
        }

        public post(url, data, cfg = null): any {
            return this.http.post(url, data, this.config(cfg));
        }

        public get(url, cfg = null): any {
            return this.http.get(url, this.config(cfg));
        }

        private config(cfg) {
            let r = {
                headers: {
                    "SecretHeader": true,
                    "Authorization": "Bearer " + this.authService.token,
                }
            };
            if (cfg) {
                $.extend(r, cfg);
            }

            return r;
        }
    }
}

((windows, angular) => {
    //we use module method or use exists module?
    angular.module("httpExApp", ["authApp"])
        .factory("httpEx", ($rootScope, $http, authService) => {
            return new Ang.HttpExService($http, authService);
        });
})(window, angular);
﻿// ReSharper disable TsResolvedFromInaccessibleModule
namespace Ang {
    export class AuthInfo {
        private grant_type: string = 'password';
        constructor(username: string, password: string) {
            this.username = username;
            this.password = password;
        }
        public username: string;
        public password: string;
        public toString(): string {
            return "grant_type=" + this.grant_type
                + "&username=" + this.username
                + "&password=" + this.password;
        }
    }

    export class AuthSerivce {

        private static TOKENKEY = "tokenInfo";

        private http:any;

        constructor(http: any) {
            this.http = http;

            let token = sessionStorage.getItem(AuthSerivce.TOKENKEY);
            if (token) {
                this.setAuth(token);
            }
        }

        public isLogged: boolean;
        public last_error:string;
        public token: string;

        public login(info: AuthInfo, onSuccess, onError) {
            this.last_error = null;
            var t = this.http.post("/Token", info.toString());
            t.success((d) => {
                this.setAuth(d.access_token);
                onSuccess();
            });
            t.error(d => {
                this.last_error = d.error_description;
                onError(d);
                //alert(d.error + "\r\n" + d.errorDescription);
            });
        }

        public logout(): void {
            this.last_error = null;
            this.setAuth(null);
        }

        public logged(): boolean {
            return this.isLogged;
        }

        private setAuth(token) {
            if (token) {
                this.isLogged = true;
                this.token = token;
                sessionStorage.setItem(AuthSerivce.TOKENKEY, token);
            } else {
                this.isLogged = false;
                this.token = null;
                sessionStorage.removeItem(AuthSerivce.TOKENKEY);
            }
        }

        public static initialize() {

        }
    }
}

((windows, angular) => {
    angular.module('authApp', []).factory('authService', ($rootScope, $http) => {
        return new Ang.AuthSerivce($http);
    });
})(window, angular);
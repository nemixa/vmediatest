// ReSharper disable TsResolvedFromInaccessibleModule
var Ang;
(function (Ang) {
    var HttpExService = (function () {
        function HttpExService(http, authSerivce) {
            this.http = http;
            this.authService = authSerivce;
        }
        HttpExService.prototype.post = function (url, data, cfg) {
            if (cfg === void 0) { cfg = null; }
            return this.http.post(url, data, this.config(cfg));
        };
        HttpExService.prototype.get = function (url, cfg) {
            if (cfg === void 0) { cfg = null; }
            return this.http.get(url, this.config(cfg));
        };
        HttpExService.prototype.config = function (cfg) {
            var r = {
                headers: {
                    "SecretHeader": true,
                    "Authorization": "Bearer " + this.authService.token,
                }
            };
            if (cfg) {
                $.extend(r, cfg);
            }
            return r;
        };
        return HttpExService;
    }());
    Ang.HttpExService = HttpExService;
})(Ang || (Ang = {}));
(function (windows, angular) {
    //we use module method or use exists module?
    angular.module("httpExApp", ["authApp"])
        .factory("httpEx", function ($rootScope, $http, authService) {
        return new Ang.HttpExService($http, authService);
    });
})(window, angular);
//# sourceMappingURL=HttpExApp.js.map
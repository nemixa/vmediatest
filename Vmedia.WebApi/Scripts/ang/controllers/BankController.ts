﻿// ReSharper disable TsResolvedFromInaccessibleModule
namespace Ang.Controllers {
    export class BankController {
        private http: HttpExService;
        public accounts: Array<Models.AccountBalance>;
        public transactions: Array<Models.Transaction>;
        public selectedAccountId: number;
        public selectedOperationId: number = 1;
        public value: number = 0;

        public clients: Array<Models.Client>;
        public rates: Array<any>;
        public targetClientId: number=0;
        public targetClient: Models.Client;

        public targetCurrency: string = "";
        public targetRate: number;
        

        private controller = this;
        private lastTranId: number;

        constructor(httpEx: Ang.HttpExService) {
            this.http = httpEx;
            this.accounts = new Array<Models.AccountBalance>();
            this.transactions = new Array<Models.Transaction>();
            this.clients = new Array<Models.Client>();
            this.rates = new Array<any>();

            this.lastTranId = 0;
            this.firstLoad();
        }

        public firstLoad() {
            this.refreshBalances();
            this.refreshTransactions();
        }

        private refreshBalances() {
            let self = this;
            this.http.get("/api/ClientBalances", null)
                .success(data => {
                    self.accounts.splice(0, self.accounts.length);
                    for (let vm of data) {
                        let item = new Ang.Models.AccountBalance(vm);
                        self.accounts.push(item);
                    }
                })
                .error(err => {
                    alert('refreshBalances error');
                });
        }

        private refreshTransactions() {
            let self = this;
            this.http.get("/api/Transactions/" + self.controller.lastTranId, null)
                .success(data => {
                    let index = 0;
                    let firstLoading = self.controller.lastTranId == 0;
                    for (let vm of data) {
                        let item = new Ang.Models.Transaction(vm);
                        if (firstLoading) {
                            self.transactions.push(item);
                        } else {
                            self.transactions.splice(index++, 0, item);
                        }
                        if (item.Id > self.controller.lastTranId) {
                            self.controller.lastTranId = item.Id;
                        }
                    }
                })
                .error(err => {
                    alert('refreshTransactions error');
                });
        }

        private refreshClients() {
            let self = this;
            this.http.get("/api/Clients/", null)
                .success(data => {
                    self.clients.splice(0, self.clients.length);
                    for (let vm of data) {
                        let item = new Models.Client(vm);
                        self.clients.push(item);
                    }
                    
                })
                .error(err => {
                    alert('refreshClients error');
                });
        }

        private refreshRates(currency:string) {
            let self = this;
            this.http.get("/api/Currency/" + currency, null)
                .success(data => {
                    self.rates.splice(0, self.rates.length);
                    for (let vm of data) {
                        self.rates.push(vm);
                    }
                })
                .error(err => {
                    alert('refreshRates error');
                });
        }

        private onSelectedAccount(op) {
            console.log("onSelectedAccount:" + this.selectedAccountId);
            let acc = this.getCurrentAccount();
            this.refreshRates(acc.currency);

            //dbg
            this.targetCurrency = acc.currency;
            this.targetRate = 1;
        }

        private onSelectedOperation(op) {
            if (this.selectedOperationId == op) {
                this.refreshClients();
            }
        }



        private onSelectedTargetClient() {
            console.log("onSelectedClient:" + this.targetClientId);
            for (let c of this.clients) {
                if (c.Id == this.targetClientId) {
                    this.targetClient = c;
                    return;
                }
            }
        }

        private onSelectedTargetCurrency() {
            console.log("onSelectedCurrency:" + this.targetCurrency);
            for (let r of this.rates) {
                if (r.Currency == this.targetCurrency) {
                    this.targetRate = r.Rate;
                    return;
                }
            }
            this.targetRate = 1;
        }

        private calculateValue() {
            return this.value * this.targetRate;
        }

        public doDisabled(opForm, ev): boolean {
            if (!opForm.$valid) return true;
            if (this.selectedOperationId != 2) {//not deposit
                if (this.getCurrentAccount().balance < this.value)
                    return true;

                if (this.selectedOperationId == 3) {
                    return this.targetClientId == 0 || this.targetCurrency == "";
                }
            }

            return false;
        }

        public doOperation(opForm) {
            let params = {
                sourceBalanceId: this.selectedAccountId,
                value: this.value,
                operationId: this.selectedOperationId,
                targetBalanceId: null,
                targetValue: null,
            }
            if (this.selectedOperationId == 3) {
                params.targetBalanceId = this.getTargetAccount().Id;
                params.targetValue = this.calculateValue();
            }
            let self = this;
            this.http.post("/api/Transactions", params)
                .success(d => {
                    if (d) {
                        self.refreshBalances();
                        self.refreshTransactions();
                    } else {
                        alert('Operation is not completed');
                    }
                })
                .error(err => {
                    alert('Operation is not completed');
                });
        }

        private getCurrentAccount(): Models.AccountBalance {
            for (let acc of this.accounts) {
                if (acc.Id == this.selectedAccountId) {
                    return acc;
                }
            }
        }

        private getTargetAccount(): Models.AccountBalance {
            if (this.targetClient) {
                for (let acc of this.targetClient.balances) {
                    if (acc.currency == this.targetCurrency)
                        return acc;
                }
            }

            return null;
        }

        public static extend($scope, httpEx) {
            let instance = new BankController(httpEx);
            $.extend($scope, instance);
        }
    }
}
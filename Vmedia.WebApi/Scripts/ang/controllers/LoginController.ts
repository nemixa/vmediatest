﻿// ReSharper disable TsResolvedFromInaccessibleModule
namespace Ang.Controllers {
    export class LoginController {
        public authService: AuthSerivce;
        private location:any;

        public error_desc:string;

        constructor(authSerivce: AuthSerivce, location) {
            this.authService = authSerivce;
            this.location = location;
        }

        public doLogin(login: string, password: string) {
            let info = new AuthInfo(login, password);
            this.authService.login(info,
                () => {
                    this.error_desc = null;
                    this.location.path('/');
                },
                (err) => {
                    this.error_desc = err.error_description;
                });
        }

        public doLogout() {
            this.authService.logout();
        }

        public static extend($scope, authService, $location) {
            let instance = new LoginController(authService, $location);
            $.extend($scope, instance);
        }
    }
}
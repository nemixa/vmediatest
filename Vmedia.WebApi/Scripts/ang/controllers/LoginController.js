// ReSharper disable TsResolvedFromInaccessibleModule
var Ang;
(function (Ang) {
    var Controllers;
    (function (Controllers) {
        var LoginController = (function () {
            function LoginController(authSerivce, location) {
                this.authService = authSerivce;
                this.location = location;
            }
            LoginController.prototype.doLogin = function (login, password) {
                var _this = this;
                var info = new Ang.AuthInfo(login, password);
                this.authService.login(info, function () {
                    _this.error_desc = null;
                    _this.location.path('/');
                }, function (err) {
                    _this.error_desc = err.error_description;
                });
            };
            LoginController.prototype.doLogout = function () {
                this.authService.logout();
            };
            LoginController.extend = function ($scope, authService, $location) {
                var instance = new LoginController(authService, $location);
                $.extend($scope, instance);
            };
            return LoginController;
        }());
        Controllers.LoginController = LoginController;
    })(Controllers = Ang.Controllers || (Ang.Controllers = {}));
})(Ang || (Ang = {}));
//# sourceMappingURL=LoginController.js.map
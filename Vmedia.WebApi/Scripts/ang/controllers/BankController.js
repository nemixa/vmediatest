// ReSharper disable TsResolvedFromInaccessibleModule
var Ang;
(function (Ang) {
    var Controllers;
    (function (Controllers) {
        var BankController = (function () {
            function BankController(httpEx) {
                this.selectedOperationId = 1;
                this.value = 0;
                this.targetClientId = 0;
                this.targetCurrency = "";
                this.controller = this;
                this.http = httpEx;
                this.accounts = new Array();
                this.transactions = new Array();
                this.clients = new Array();
                this.rates = new Array();
                this.lastTranId = 0;
                this.firstLoad();
            }
            BankController.prototype.firstLoad = function () {
                this.refreshBalances();
                this.refreshTransactions();
            };
            BankController.prototype.refreshBalances = function () {
                var self = this;
                this.http.get("/api/ClientBalances", null)
                    .success(function (data) {
                    self.accounts.splice(0, self.accounts.length);
                    for (var _i = 0, data_1 = data; _i < data_1.length; _i++) {
                        var vm = data_1[_i];
                        var item = new Ang.Models.AccountBalance(vm);
                        self.accounts.push(item);
                    }
                })
                    .error(function (err) {
                    alert('refreshBalances error');
                });
            };
            BankController.prototype.refreshTransactions = function () {
                var self = this;
                this.http.get("/api/Transactions/" + self.controller.lastTranId, null)
                    .success(function (data) {
                    var index = 0;
                    var firstLoading = self.controller.lastTranId == 0;
                    for (var _i = 0, data_2 = data; _i < data_2.length; _i++) {
                        var vm = data_2[_i];
                        var item = new Ang.Models.Transaction(vm);
                        if (firstLoading) {
                            self.transactions.push(item);
                        }
                        else {
                            self.transactions.splice(index++, 0, item);
                        }
                        if (item.Id > self.controller.lastTranId) {
                            self.controller.lastTranId = item.Id;
                        }
                    }
                })
                    .error(function (err) {
                    alert('refreshTransactions error');
                });
            };
            BankController.prototype.refreshClients = function () {
                var self = this;
                this.http.get("/api/Clients/", null)
                    .success(function (data) {
                    self.clients.splice(0, self.clients.length);
                    for (var _i = 0, data_3 = data; _i < data_3.length; _i++) {
                        var vm = data_3[_i];
                        var item = new Ang.Models.Client(vm);
                        self.clients.push(item);
                    }
                })
                    .error(function (err) {
                    alert('refreshClients error');
                });
            };
            BankController.prototype.refreshRates = function (currency) {
                var self = this;
                this.http.get("/api/Currency/" + currency, null)
                    .success(function (data) {
                    self.rates.splice(0, self.rates.length);
                    for (var _i = 0, data_4 = data; _i < data_4.length; _i++) {
                        var vm = data_4[_i];
                        self.rates.push(vm);
                    }
                })
                    .error(function (err) {
                    alert('refreshRates error');
                });
            };
            BankController.prototype.onSelectedAccount = function (op) {
                console.log("onSelectedAccount:" + this.selectedAccountId);
                var acc = this.getCurrentAccount();
                this.refreshRates(acc.currency);
                //dbg
                this.targetCurrency = acc.currency;
                this.targetRate = 1;
            };
            BankController.prototype.onSelectedOperation = function (op) {
                if (this.selectedOperationId == op) {
                    this.refreshClients();
                }
            };
            BankController.prototype.onSelectedTargetClient = function () {
                console.log("onSelectedClient:" + this.targetClientId);
                for (var _i = 0, _a = this.clients; _i < _a.length; _i++) {
                    var c = _a[_i];
                    if (c.Id == this.targetClientId) {
                        this.targetClient = c;
                        return;
                    }
                }
            };
            BankController.prototype.onSelectedTargetCurrency = function () {
                console.log("onSelectedCurrency:" + this.targetCurrency);
                for (var _i = 0, _a = this.rates; _i < _a.length; _i++) {
                    var r = _a[_i];
                    if (r.Currency == this.targetCurrency) {
                        this.targetRate = r.Rate;
                        return;
                    }
                }
                this.targetRate = 1;
            };
            BankController.prototype.calculateValue = function () {
                return this.value * this.targetRate;
            };
            BankController.prototype.doDisabled = function (opForm, ev) {
                if (!opForm.$valid)
                    return true;
                if (this.selectedOperationId != 2) {
                    if (this.getCurrentAccount().balance < this.value)
                        return true;
                    if (this.selectedOperationId == 3) {
                        return this.targetClientId == 0 || this.targetCurrency == "";
                    }
                }
                return false;
            };
            BankController.prototype.doOperation = function (opForm) {
                var params = {
                    sourceBalanceId: this.selectedAccountId,
                    value: this.value,
                    operationId: this.selectedOperationId,
                    targetBalanceId: null,
                    targetValue: null,
                };
                if (this.selectedOperationId == 3) {
                    params.targetBalanceId = this.getTargetAccount().Id;
                    params.targetValue = this.calculateValue();
                }
                var self = this;
                this.http.post("/api/Transactions", params)
                    .success(function (d) {
                    if (d) {
                        self.refreshBalances();
                        self.refreshTransactions();
                    }
                    else {
                        alert('Operation is not completed');
                    }
                })
                    .error(function (err) {
                    alert('Operation is not completed');
                });
            };
            BankController.prototype.getCurrentAccount = function () {
                for (var _i = 0, _a = this.accounts; _i < _a.length; _i++) {
                    var acc = _a[_i];
                    if (acc.Id == this.selectedAccountId) {
                        return acc;
                    }
                }
            };
            BankController.prototype.getTargetAccount = function () {
                if (this.targetClient) {
                    for (var _i = 0, _a = this.targetClient.balances; _i < _a.length; _i++) {
                        var acc = _a[_i];
                        if (acc.currency == this.targetCurrency)
                            return acc;
                    }
                }
                return null;
            };
            BankController.extend = function ($scope, httpEx) {
                var instance = new BankController(httpEx);
                $.extend($scope, instance);
            };
            return BankController;
        }());
        Controllers.BankController = BankController;
    })(Controllers = Ang.Controllers || (Ang.Controllers = {}));
})(Ang || (Ang = {}));
//# sourceMappingURL=BankController.js.map
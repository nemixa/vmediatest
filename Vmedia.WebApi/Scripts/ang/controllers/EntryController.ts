﻿// ReSharper disable TsResolvedFromInaccessibleModule
namespace Ang.Controllers {
    export class Link {
        public route:any;
        public path: string;
        public title: string;
    }

    export class EntryController {
        private route: any;
        private authService: AuthSerivce;
        private controller: EntryController = this;

        public links: Array<Link>;

        constructor(route:any, authService:AuthSerivce) {
            this.route = route;
            this.authService = authService;

            this.links = new Array<Link>();
            for (let path in route.routes) {
                let r = route.routes[path];
                let l = new Link();
                l.route = r;
                l.path = path;
                l.title = r.title;
                this.links.push(l);
            }
        }

        public visible(link: Link): boolean {
            if (link.route.auth) {
                return this.authService.logged();
            } 
            if (link.route.noauth) {
                return !this.authService.logged();
            }

            return true;
        }

        public static extend($scope, authService, $route) {
            let instance = new EntryController($route, authService);
            $.extend($scope, instance);
        }
    }
}
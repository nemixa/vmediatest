// ReSharper disable TsResolvedFromInaccessibleModule
var Ang;
(function (Ang) {
    var Controllers;
    (function (Controllers) {
        var Link = (function () {
            function Link() {
            }
            return Link;
        }());
        Controllers.Link = Link;
        var EntryController = (function () {
            function EntryController(route, authService) {
                this.controller = this;
                this.route = route;
                this.authService = authService;
                this.links = new Array();
                for (var path in route.routes) {
                    var r = route.routes[path];
                    var l = new Link();
                    l.route = r;
                    l.path = path;
                    l.title = r.title;
                    this.links.push(l);
                }
            }
            EntryController.prototype.visible = function (link) {
                if (link.route.auth) {
                    return this.authService.logged();
                }
                if (link.route.noauth) {
                    return !this.authService.logged();
                }
                return true;
            };
            EntryController.extend = function ($scope, authService, $route) {
                var instance = new EntryController($route, authService);
                $.extend($scope, instance);
            };
            return EntryController;
        }());
        Controllers.EntryController = EntryController;
    })(Controllers = Ang.Controllers || (Ang.Controllers = {}));
})(Ang || (Ang = {}));
//# sourceMappingURL=EntryController.js.map
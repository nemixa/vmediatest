﻿// ReSharper disable TsResolvedFromInaccessibleModule
namespace Ang.Controllers {
    export class WelcomeController {
        public static extend($scope) {
            let instance = new WelcomeController();
            $.extend($scope, instance);
        }
    }
}
// ReSharper disable TsResolvedFromInaccessibleModule
var Ang;
(function (Ang) {
    var Controllers;
    (function (Controllers) {
        var WelcomeController = (function () {
            function WelcomeController() {
            }
            WelcomeController.extend = function ($scope) {
                var instance = new WelcomeController();
                $.extend($scope, instance);
            };
            return WelcomeController;
        }());
        Controllers.WelcomeController = WelcomeController;
    })(Controllers = Ang.Controllers || (Ang.Controllers = {}));
})(Ang || (Ang = {}));
//# sourceMappingURL=WelcomeController.js.map
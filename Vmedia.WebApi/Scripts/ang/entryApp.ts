﻿//-/// <reference path="../typings/angularjs/angular.d.ts" />
// ReSharper disable TsResolvedFromInaccessibleModule
//namespace ang {
//    export class Entry {
//        constructor() {

//        }

//        public static isLogin(): boolean {
//            return sessionStorage.getItem("KKEY");
//        }

//        public static initialize() {
//            var entryApp = angular.module('entryApp', []);

//            entryApp.run($rootScope => {
//                let x = sessionStorage.getItem("KKEY");
//                if (x) {
//                    alert(x);
//                    //load content
//                } else {



//                    //loginApp.controller('loginController', $scope => {

//                    //    $scope.enter = login => {
//                    //        sessionStorage.setItem("KKEY", login);
//                    //    };
//                    //});
//                    //load login
//                }
//            });

//            var loginApp = angular.module("loginApp", ['entryApp']);
//            loginApp.run($rootScope => {
//                $rootScope.viewUrl = () => {
//                    if (!Entry.isLogin())
//                        return "/Home/Login";
//                    else return "";
//                };

//            });
//        }

//    }
//}

//$(() => {
//    alert('atttttaaa');
//});

namespace Ang {
    export class EntryApp {
        constructor() {

        }

        public static initialize(angular: any) {
            var module = angular.module('entryApp', ['authApp', 'httpExApp', 'ngRoute']);
        }

        private static configRoutes(module: any) {
        }
    }

}


((windows, angular) => {
    let entry = angular.module('entryApp', ['authApp', 'httpExApp', 'ngRoute']);
    entry.config(($routeProvider, $locationProvider, $httpProvider) => {
        $routeProvider
            .when('/', {  templateUrl: '/Home/Welcome', title:'Welcome' })
            .when('/login/', { templateUrl: '/Home/Login', title: 'Login', noauth:true})
            .when('/bank/', { templateUrl: '/Home/Bank', title: 'Bank', auth: true })
            .when('/logout/', { templateUrl: '/Home/Login', title: 'Logout', auth: true })
            .otherwise({ redirectTo: '/' });
        });

    entry.run(($rootScope, $templateCache, $location, authService) => {
        // UserService.run({ urlLogin: 'login.json', urlLogout: "login.json" });
        $rootScope.$on("$routeChangeStart", (event, next, current) => {
            if (typeof (current) !== 'undefined') {
                $templateCache.remove(current.templateUrl);
                if (next.auth) {
                    if (!authService.logged()) {
                        $location.path('/login');
                    }
                }
            }
        });
    });

    entry.controller("EntryController", Ang.Controllers.EntryController.extend);
    entry.controller("LoginController", Ang.Controllers.LoginController.extend);
    entry.controller("WelcomeController", Ang.Controllers.WelcomeController.extend);
    entry.controller("BankController", Ang.Controllers.BankController.extend);

})(window, angular);



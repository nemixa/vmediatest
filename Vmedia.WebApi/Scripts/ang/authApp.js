// ReSharper disable TsResolvedFromInaccessibleModule
var Ang;
(function (Ang) {
    var AuthInfo = (function () {
        function AuthInfo(username, password) {
            this.grant_type = 'password';
            this.username = username;
            this.password = password;
        }
        AuthInfo.prototype.toString = function () {
            return "grant_type=" + this.grant_type
                + "&username=" + this.username
                + "&password=" + this.password;
        };
        return AuthInfo;
    }());
    Ang.AuthInfo = AuthInfo;
    var AuthSerivce = (function () {
        function AuthSerivce(http) {
            this.http = http;
            var token = sessionStorage.getItem(AuthSerivce.TOKENKEY);
            if (token) {
                this.setAuth(token);
            }
        }
        AuthSerivce.prototype.login = function (info, onSuccess, onError) {
            var _this = this;
            this.last_error = null;
            var t = this.http.post("/Token", info.toString());
            t.success(function (d) {
                _this.setAuth(d.access_token);
                onSuccess();
            });
            t.error(function (d) {
                _this.last_error = d.error_description;
                onError(d);
                //alert(d.error + "\r\n" + d.errorDescription);
            });
        };
        AuthSerivce.prototype.logout = function () {
            this.last_error = null;
            this.setAuth(null);
        };
        AuthSerivce.prototype.logged = function () {
            return this.isLogged;
        };
        AuthSerivce.prototype.setAuth = function (token) {
            if (token) {
                this.isLogged = true;
                this.token = token;
                sessionStorage.setItem(AuthSerivce.TOKENKEY, token);
            }
            else {
                this.isLogged = false;
                this.token = null;
                sessionStorage.removeItem(AuthSerivce.TOKENKEY);
            }
        };
        AuthSerivce.initialize = function () {
        };
        AuthSerivce.TOKENKEY = "tokenInfo";
        return AuthSerivce;
    }());
    Ang.AuthSerivce = AuthSerivce;
})(Ang || (Ang = {}));
(function (windows, angular) {
    angular.module('authApp', []).factory('authService', function ($rootScope, $http) {
        return new Ang.AuthSerivce($http);
    });
})(window, angular);
//# sourceMappingURL=AuthApp.js.map
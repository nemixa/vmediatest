﻿namespace Ang.Models {
    export class Transaction extends Base {
        public user: string;
        public amount: number;
        public operation: string;
        public toUser: string;
        public date: string;
        public currency: string;
        public value: string;

        constructor(vm:any) {
            super();

            this.Id = vm.Id;
            this.user = vm.SourceBalance.Client.Login;
            this.amount = vm.SourceValue;

            switch (vm.OperationId) {
            case 1:
                this.operation = "Withdraw";
                break;
            case 2:
                this.operation = "Deposit";
                break;
            case 3:
                this.operation = "Transfer";
                break;
            }

            if (vm.TargetBalance) {
                this.toUser = vm.TargetBalance.Client.Login;
                this.value = vm.TargetBalance.Currency + " " + vm.TargetValue;
            } else {
                this.toUser = "";
                this.value = "";
            }

            this.date = vm.Date;

            this.currency = vm.SourceBalance.Currency;
        }

    }
}
﻿namespace Ang.Models {
    export class Client extends Base {
        public login: string;
        public balances: Array<AccountBalance>;

        constructor(vm: any) {
            super();

            this.Id = vm.Id;
            this.login = vm.Login;
            this.balances = new Array<AccountBalance>();
            for (let b of vm.Balances) {
                let item = new AccountBalance(b);
                this.balances.push(item);
            }
        }
    }
}
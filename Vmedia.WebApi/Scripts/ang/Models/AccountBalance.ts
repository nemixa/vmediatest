﻿namespace Ang.Models {
    export class AccountBalance extends Base {
        public currency: string;
        public balance: number;
        public title:string;

        constructor(vm:any) {
            super();

            this.Id = vm.Id;
            this.currency = vm.Currency;
            this.balance = vm.Amount;
        }


    }
}
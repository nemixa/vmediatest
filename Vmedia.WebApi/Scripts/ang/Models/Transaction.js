var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Ang;
(function (Ang) {
    var Models;
    (function (Models) {
        var Transaction = (function (_super) {
            __extends(Transaction, _super);
            function Transaction(vm) {
                _super.call(this);
                this.Id = vm.Id;
                this.user = vm.SourceBalance.Client.Login;
                this.amount = vm.SourceValue;
                switch (vm.OperationId) {
                    case 1:
                        this.operation = "Withdraw";
                        break;
                    case 2:
                        this.operation = "Deposit";
                        break;
                    case 3:
                        this.operation = "Transfer";
                        break;
                }
                if (vm.TargetBalance) {
                    this.toUser = vm.TargetBalance.Client.Login;
                    this.value = vm.TargetBalance.Currency + " " + vm.TargetValue;
                }
                else {
                    this.toUser = "";
                    this.value = "";
                }
                this.date = vm.Date;
                this.currency = vm.SourceBalance.Currency;
            }
            return Transaction;
        }(Models.Base));
        Models.Transaction = Transaction;
    })(Models = Ang.Models || (Ang.Models = {}));
})(Ang || (Ang = {}));
//# sourceMappingURL=Transaction.js.map
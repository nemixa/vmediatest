var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Ang;
(function (Ang) {
    var Models;
    (function (Models) {
        var Client = (function (_super) {
            __extends(Client, _super);
            function Client(vm) {
                _super.call(this);
                this.Id = vm.Id;
                this.login = vm.Login;
                this.balances = new Array();
                for (var _i = 0, _a = vm.Balances; _i < _a.length; _i++) {
                    var b = _a[_i];
                    var item = new Models.AccountBalance(b);
                    this.balances.push(item);
                }
            }
            return Client;
        }(Models.Base));
        Models.Client = Client;
    })(Models = Ang.Models || (Ang.Models = {}));
})(Ang || (Ang = {}));
//# sourceMappingURL=Client.js.map
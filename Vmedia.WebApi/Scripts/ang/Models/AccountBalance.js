var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Ang;
(function (Ang) {
    var Models;
    (function (Models) {
        var AccountBalance = (function (_super) {
            __extends(AccountBalance, _super);
            function AccountBalance(vm) {
                _super.call(this);
                this.Id = vm.Id;
                this.currency = vm.Currency;
                this.balance = vm.Amount;
            }
            return AccountBalance;
        }(Models.Base));
        Models.AccountBalance = AccountBalance;
    })(Models = Ang.Models || (Ang.Models = {}));
})(Ang || (Ang = {}));
//# sourceMappingURL=AccountBalance.js.map
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vmedia.WebApi.DTO
{
    public class BalanceDto
    {
        public int Id { get; set; }
        public string Currency { get; set; }
    }
}
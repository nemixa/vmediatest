﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vmedia.Data;

namespace Vmedia.WebApi.DTO
{
    public class OperationDto
    {
        public int SourceBalanceId { get; set; }
        public decimal Value { get; set; }
        public TransactionType OperationId { get; set; }
        public int? TargetBalanceId { get; set; }
        public decimal? TargetValue { get; set; }
    }
}
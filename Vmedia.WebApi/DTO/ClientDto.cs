﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vmedia.Data.Entities;

namespace Vmedia.WebApi.DTO
{
    public class ClientDto
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public List<BalanceDto> Balances { get; set; }
    }
}
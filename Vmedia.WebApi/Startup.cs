﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Owin;
using Owin;
using Vmedia.WebApi.App_Start;
using Vmedia.WebApi.Owin;

[assembly: OwinStartup(typeof(Vmedia.WebApi.Startup))]

namespace Vmedia.WebApi
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.UseOwinExceptionHandler();
            AutofacConfig.ConfigureForWebApi(app);
            ConfigureAuth(app);
        }
    }
}

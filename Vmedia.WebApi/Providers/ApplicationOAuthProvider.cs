﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Owin;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Infrastructure;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using Vmedia.Data;
using Vmedia.Data.Entities;
using Vmedia.Data.Imp;
using Vmedia.Data.Repositories;
using Vmedia.WebApi.Helpers;

namespace Vmedia.WebApi.Providers
{
    public class ApplicationOAuthProvider : OAuthAuthorizationServerProvider
    {
        private IClientsRepository _clientsRepository;

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            if (_clientsRepository == null)
            {
                _clientsRepository = context.OwinContext.GetAutofacLifetimeScope().Resolve<IClientsRepository>();
            }

            ClaimsIdentity oAuthIdentity = new ClaimsIdentity(OAuthDefaults.AuthenticationType);
            ClaimsIdentity cookiesIdentity = new ClaimsIdentity(CookieAuthenticationDefaults.AuthenticationType);

            var client = _clientsRepository.Get(context.UserName, context.Password);
            if (client == null)
            {
                context.SetError("Authorization Error", "The username or password is incorrect!");
                return;
            }

            oAuthIdentity.AddClaim(new Claim(ClaimTypes.Name, client.Login));
            oAuthIdentity.AddClaim(new Claim(ClaimTypes.Email, client.Email));
            oAuthIdentity.AddClaim(new Claim(ClaimHelper.IdType, client.Id.ToString()));

            AuthenticationProperties properties = CreateProperties(client.Login);
            AuthenticationTicket ticket = new AuthenticationTicket(oAuthIdentity, properties);
            context.Validated(ticket);
            context.Request.Context.Authentication.SignIn(cookiesIdentity, oAuthIdentity);

        }

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
            return Task.FromResult<object>(null);
        }

        #region Auxiliary methods

        private static AuthenticationProperties CreateProperties(string userName)
        {
            IDictionary<string, string> data = new Dictionary<string, string>
            {
                { "userName", userName }
            };
            return new AuthenticationProperties(data)
            {
                IsPersistent = true,
            };
        }

        #endregion
    }
}
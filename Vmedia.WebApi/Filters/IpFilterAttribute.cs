﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Web.Http.Results;
using System.Web.Mvc;
using Microsoft.Owin;
using Vmedia.WebApi.Extensions;


namespace Vmedia.WebApi.Filters
{
    public class IpFilterAttribute: Attribute, IAuthenticationFilter
    {
        public const string ConfigName = "IpWhiteList";

        #region Implementation of IAuthenticationFilter

        public bool AllowMultiple => false;

        public Task AuthenticateAsync(HttpAuthenticationContext context, CancellationToken cancellationToken)
        {
            string ip = context.Request.GetClientIpAddress();
            if (ip == null || !CheckIp(ip))
            {
                context.ErrorResult = new ResponseMessageResult(new HttpResponseMessage(HttpStatusCode.Unauthorized)
                {
                    Content = new StringContent("Your IP is not allowed")
                });
            }

            return Task.FromResult<object>(null);
        }

        public Task ChallengeAsync(HttpAuthenticationChallengeContext context, CancellationToken cancellationToken)
        {
            return Task.FromResult<object>(null);
        }

        #endregion

        private static bool CheckIp(string ipStr)
        {
            string[] ips = GetIpList();
            return ips.Any(ip => ip == ipStr);
        }

        private static string[] GetIpList()
        {
            string value = ConfigurationManager.AppSettings[ConfigName];
            if (string.IsNullOrEmpty(value))
            {
                return new string[0];
            }

            return value.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries);
        }

    }
}




//public void OnAuthentication(AuthenticationContext filterContext)
//{
//    string ip = filterContext.HttpContext.Request.UserHostAddress;
//    if (ip == null || !CheckIp(ip))
//    {
//        filterContext.Result = new HttpUnauthorizedResult();
//    }
//}

//public void OnAuthenticationChallenge(AuthenticationChallengeContext filterContext)
//{

//}


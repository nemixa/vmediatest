﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FilterAttribute = System.Web.Http.Filters.FilterAttribute;

namespace Vmedia.WebApi.Filters
{
    public class UserAgentFilterAttribute : FilterAttribute, IActionFilter
    {
        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var request = filterContext.HttpContext.Request;
            var isIe9Compatible = false;

            if (request.UrlReferrer == null)
            {
                if (request.Browser.Browser.Trim().ToUpperInvariant().Equals("IE") && request.Browser.MajorVersion <= 8)
                {
                    var useragent = request.Headers.GetValues("User-Agent");
                    if (useragent != null) isIe9Compatible = useragent[0].Contains("Trident/5.0");
                    if (!isIe9Compatible) filterContext.Result = new ViewResult { ViewName = "_OldBrowserWarning" };
                }
            }
        }

        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
        }
    }
}
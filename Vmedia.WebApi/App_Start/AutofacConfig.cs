﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.WebApi;
using Owin;
using Vmedia.Data;
using Vmedia.Data.Entities;
using Vmedia.Data.Imp;
using Vmedia.Data.Repositories;

namespace Vmedia.WebApi.App_Start
{
    public static class AutofacConfig
    {
        public static void ConfigureForWebApi(IAppBuilder app)
        {
            var builder = new ContainerBuilder();
            HttpConfiguration cfg = GlobalConfiguration.Configuration;//new HttpConfiguration();//
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            RegisterTypes(builder);

            var container = builder.Build();
            cfg.DependencyResolver = new AutofacWebApiDependencyResolver(container);

            app.UseAutofacMiddleware(container);
            app.UseAutofacWebApi(cfg);
        }

        private static void RegisterTypes(ContainerBuilder builder)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

            builder.RegisterType<ClientBalancesRepository>()
                .As<IClientBalancesRepository>()
                .WithParameter("connectionString", connectionString);

            builder.RegisterType<ClientsesRepository>()
                .As<IClientsRepository>()
                .WithParameter("connectionString", connectionString);

            builder.RegisterType<TransactionsRepository>()
                .As<ITransactionsRepository>()
                .WithParameter("connectionString", connectionString);
        }
    }
}
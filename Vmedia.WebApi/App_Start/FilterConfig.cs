﻿using System.Web;
using System.Web.Mvc;
using Vmedia.WebApi.Filters;

namespace Vmedia.WebApi
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());

            filters.Add(new UserAgentFilterAttribute());
        }
    }
}

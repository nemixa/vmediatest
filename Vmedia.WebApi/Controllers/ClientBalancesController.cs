﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Vmedia.Data;
using Vmedia.Data.Entities;
using Vmedia.Data.Imp;
using Vmedia.WebApi.Helpers;

namespace Vmedia.Web.Controllers
{
    [Authorize]
    public class ClientBalancesController : ApiController
    {
        private readonly IClientBalancesRepository _clientBalanceRepository;

        public ClientBalancesController(IClientBalancesRepository clientBalanceRepository)
        {
            _clientBalanceRepository = clientBalanceRepository;
        }

        // GET: api/Balances
        public IEnumerable<ClientBalance> Get()
        {
            var userId = this.RequestContext.Principal.Identity.GetClientId();
            var items = _clientBalanceRepository.GetForClient(userId).ToArray();
            return items;
        }
    }
}

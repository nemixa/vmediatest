﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Vmedia.WebApi.Helpers;

namespace Vmedia.WebApi.Controllers
{
    [Authorize]
    public class CurrencyController : ApiController
    {
        private readonly static string[] Currencies = { "USD","EUR","CAD" };

        [Route("api/Currency/{currency}")]
        public IEnumerable<ExchangeRate> Get(string currency)
        {
            return ExchangeRate.GetRates(currency).Where(c => Currencies.Contains(c.Currency));
        }
    }
}

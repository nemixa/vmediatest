﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vmedia.Data;
using Vmedia.Data.Imp;
using Vmedia.Data.Repositories;
using Vmedia.WebApi.Filters;
using Vmedia.WebApi.Helpers;

namespace Vmedia.WebApi.Controllers
{
    public class HomeController : Controller
    {
        //App
        public ActionResult Index()
        {
            return View();
        }

        //Login
        public PartialViewResult Login()
        {
            return PartialView();
        }

        //Welcome
        public PartialViewResult Welcome()
        {
            return PartialView();
        }

        //Bank
        public PartialViewResult Bank()
        {
            string browser = Request.Browser.Browser.ToLower();
            switch (browser)
            {
                case "chrome":
                    ViewBag.css = "chromeClass";
                    break;
                case "firefox":
                    ViewBag.css = "firefoxClass";
                    break;
                default:
                    ViewBag.css = "defaultClass";
                    break;
            }

            return PartialView("Bank");
        }
    }
}

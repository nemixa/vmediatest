﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Vmedia.Data.Entities;
using Vmedia.Data.Imp;
using Vmedia.WebApi.DTO;
using Vmedia.WebApi.Helpers;

namespace Vmedia.WebApi.Controllers
{
    [Authorize]
    public class ClientsController : ApiController
    {
        private readonly IClientsRepository _clientRepository;
        public ClientsController(IClientsRepository clientRepository)
        {
            _clientRepository = clientRepository;
        }

        [HttpGet]
        public IEnumerable<ClientDto> Get()
        {
            var userId = this.RequestContext.Principal.Identity.GetClientId();
            var itemsQuery = _clientRepository.GetAllClients()
                //.Where(q => q.Id != userId)
                .ToArray();

            IEnumerable<ClientDto> result = itemsQuery.Select(t => new ClientDto
            {
                Id = t.Id,
                Login = t.Login,
                Balances = t.Balances.Select(b=>new BalanceDto
                {
                   Currency = b.Currency,
                   Id = b.Id, 
                }).ToList(),
            });

            return result;
        }
    }
}
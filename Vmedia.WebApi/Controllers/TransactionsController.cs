﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Vmedia.Data;
using Vmedia.Data.Entities;
using Vmedia.Data.Imp;
using Vmedia.Data.Repositories;
using Vmedia.WebApi.DTO;
using Vmedia.WebApi.Helpers;

namespace Vmedia.WebApi.Controllers
{
    [Authorize]
    public class TransactionsController : ApiController
    {
        private readonly ITransactionsRepository _transactionsRepository;
        private readonly IClientBalancesRepository _clientBalancesRepository;

        public TransactionsController(ITransactionsRepository transactionsRepository, IClientBalancesRepository clientBalanceRepository)
        {
            _transactionsRepository = transactionsRepository;
            _clientBalancesRepository = clientBalanceRepository;
        }

        [Route("api/Transactions/{lastTransactionId}")]
        public IEnumerable<Transaction> Get(int lastTransactionId)
        {
            var userId = this.RequestContext.Principal.Identity.GetClientId();
            Transaction[] result = _transactionsRepository.GetListForClient(userId, lastTransactionId).ToArray();
            return result;
        }

        // POST: api/Transaction
        public object Post([FromBody]OperationDto op)
        {
            if (op.Value <= 0) return false;
  

            var userId = this.RequestContext.Principal.Identity.GetClientId();
            try
            {
                var balance = _clientBalancesRepository.Get(op.SourceBalanceId);
                if (balance == null || balance.ClientId != userId)
                {
                    return false;
                }

                if (op.Value > balance.Amount)
                {
                    throw new ApplicationException($"{balance.Amount} greater than {op.Value}");
                }

                ClientBalance targetBalance = null;
                if (op.TargetBalanceId.HasValue)
                {
                    targetBalance = _clientBalancesRepository.Get(op.TargetBalanceId.Value);
                    if (targetBalance == null)
                    {
                        return false;
                    }
                }

                if (op.TargetValue.HasValue && targetBalance != null && targetBalance.Currency != balance.Currency)
                {
                    if (CheckTarget(balance.Currency, targetBalance.Currency, op.Value, op.TargetValue.Value))
                    {
                        //NOTE: Поидее нужно вводить коды ошибок, чтобы соответствующе реагировать на клиенте. Например, в данном случае перезарузить рейты при получении ошибки
                        //Но в целях экономии времени я этого предпочел не делать. Хотя да. Саму проверку на изменение курсов за время операции тоже можно было не делать сейчас
                        throw new ApplicationException("Rate was changed");
                    }
                }

                _transactionsRepository.AddTransaction(new TransactionsRepository.OperationParams
                {
                    OperationId = op.OperationId,
                    SourceValue = op.Value,
                    SourceBalanceId = op.SourceBalanceId,
                    TargetBalanceId = op.TargetBalanceId,
                    TargetValue = op.TargetValue,
                });
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return true;
        }

        private static bool CheckTarget(string baseCurrency, string toCurrency, decimal value, decimal receivedTargetValue)
        {
            var list = ExchangeRate.GetRates(baseCurrency);
            var target = list.SingleOrDefault(c => c.Currency == toCurrency);
            if (target == null)
            {
                throw new ApplicationException($"No rate for {toCurrency}");
            }

            decimal targetValue = target.Rate* value;
            return receivedTargetValue != targetValue;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;
using Microsoft.AspNet.Identity;

namespace Vmedia.WebApi.Helpers
{
    public static class ClaimHelper
    {
        public const string IdType = "Id";

        public static string Get(this IIdentity principal, string type)
        {
            var identity = principal as ClaimsIdentity;
            if (identity == null)
            {
                throw new ApplicationException("Not claimsIdentity");
            }

            return identity.FindFirstValue(IdType);
        }

        public static int GetClientId(this IIdentity principal)
        {
            return int.Parse(Get(principal, IdType));
        }
    }
}
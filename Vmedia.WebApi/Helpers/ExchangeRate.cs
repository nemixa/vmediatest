﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;

namespace Vmedia.WebApi.Helpers
{
    public class ExchangeRate
    {
        public string Currency { get; private set; }
        public decimal Rate { get; private set; }

        private ExchangeRate(string currency, decimal rate)
        {
            Currency = currency;
            Rate = rate;
        }

        //private static readonly Dictionary<string, ExchangeRate[]> Rates;

        //static ExchangeRate()
        //{
        //    Rates = new Dictionary<string, ExchangeRate[]>();

        //    Rates.Add("USD", new[] { new ExchangeRate("CAD", 0.96m), new ExchangeRate("EUR", 1.05m), });
        //    Rates.Add("CAD", new[] { new ExchangeRate("USD", 0.96m), new ExchangeRate("EUR", 1.05m), });
        //    Rates.Add("EUR", new[] { new ExchangeRate("USD", 0.96m), new ExchangeRate("CAD", 1.05m), });
        //}

        //public static IEnumerable<ExchangeRate> GetRates(string currency)
        //{
        //    return Rates[currency];
        //}

        public static List<ExchangeRate> GetRates(string currency)
        {
            using (System.Net.WebClient client = new System.Net.WebClient())
            {
                string json = client.DownloadString($"http://api.fixer.io/latest?base={currency}");
                var entry = Json.Decode<JsonEntry>(json);
                return entry.rates
                    .Select(t => new ExchangeRate(t.Key, t.Value)).ToList();
            }
        }

        private class JsonEntry
        {
            public Dictionary<string, decimal> rates;
        }
    }
}
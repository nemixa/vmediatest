﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vmedia.Data
{
    public abstract class RepositoryBase
    {
        public string ConnectionString { get; }

        protected RepositoryBase(string connectionString)
        {
            ConnectionString = connectionString;
        }

        protected internal IDbConnection CreateSqlConnection()
        {
            return new SqlConnection(ConnectionString);
        }
    }

    public abstract class RepositoryBase<T> : RepositoryBase
        where T : EntityBase
    {
        protected RepositoryBase(string connectionString) : base(connectionString)
        {

        }

        public virtual T Get()
        {
            throw new NotImplementedException();
        }
    }
}

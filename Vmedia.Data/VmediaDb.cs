﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vmedia.Data.Repositories;

namespace Vmedia.Data
{
    internal class VmediaDb
    {
        private readonly string _connectionString;
        public VmediaDb(string connectionString)
        {
            _connectionString = connectionString;
        }

        public VmediaDb()
            :this(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString)
        {
        }

        private static VmediaDb _instance;
        public static VmediaDb Current => _instance ?? (_instance = new VmediaDb());

        private ClientsesRepository _clientses;
        public ClientsesRepository Clientses => _clientses ?? (_clientses = new ClientsesRepository(_connectionString));

        private ClientBalancesRepository _clientBalances;
        public ClientBalancesRepository ClientBalances => _clientBalances ?? (_clientBalances = new ClientBalancesRepository(_connectionString));


        private TransactionsRepository _transactions;
        public TransactionsRepository Transactions => _transactions ?? (_transactions = new TransactionsRepository(_connectionString));
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vmedia.Data.Entities
{
    public class Transaction:EntityBase
    {
        public override object Key => Id;

        public long Id { get; set; }
        public int SourceBalanceId { get; set; }
        public int? TargetBalanceId { get; set; }
        public TransactionType OperationId { get; set; }

        public decimal SourceValue { get; set; }
        public decimal? TargetValue { get; set; }
        public DateTime Date { get; set; }

        public ClientBalance SourceBalance { get; set; }
        public ClientBalance TargetBalance { get; set; }
    }
}

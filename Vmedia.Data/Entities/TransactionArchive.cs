﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vmedia.Data.Entities
{
    public class TransactionArchive:Transaction
    {
        public DateTime ArchiveDate { get; set; }
    }
}

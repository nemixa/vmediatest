﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vmedia.Data.Entities
{
    public class Client : EntityBase
    {
        public override object Key => Id;

        public int Id { get; set; }
        public string Login { get; set; }
        public string Email { get; set; }
        public DateTime RegDate { get; set; }

        public List<ClientBalance> Balances { get; set; }
    }
}

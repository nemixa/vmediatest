﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vmedia.Data.Entities
{
    public class ClientBalance : EntityBase
    {
        public override object Key => Id;

        public int Id { get; set; }
        public int ClientId { get; set; }
        public string Currency { get; set; }
        public decimal Amount { get; set; }

        public Client Client { get; set; }
    }
}

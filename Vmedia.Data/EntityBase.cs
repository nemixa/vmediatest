﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vmedia.Data
{
    public abstract class EntityBase
    {
        public abstract object Key { get; }
    }
}

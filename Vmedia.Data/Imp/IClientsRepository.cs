﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vmedia.Data.Entities;

namespace Vmedia.Data.Imp
{
    public interface IClientsRepository
    {
        Client Get(string login, string password);
        IEnumerable<Client> GetAllClients();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vmedia.Data.Entities;
using Vmedia.Data.Repositories;

namespace Vmedia.Data.Imp
{
    public interface ITransactionsRepository
    {
        IEnumerable<Transaction> GetListForClient(int clientId, int transactionLastId);
        void AddTransaction(TransactionsRepository.OperationParams options);
    }
}

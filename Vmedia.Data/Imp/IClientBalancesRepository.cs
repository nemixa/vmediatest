﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vmedia.Data.Entities;

namespace Vmedia.Data.Imp
{
    public interface IClientBalancesRepository
    {
        IEnumerable<ClientBalance> GetForClient(int clientId);
        ClientBalance Get(int id);
    }
}

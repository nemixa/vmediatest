﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vmedia.Data
{
    public enum TransactionType
    {
        Withdraw = 1,
        Deposit = 2,
        Transfer = 3,
    }
}

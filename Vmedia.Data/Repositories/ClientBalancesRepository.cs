﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Vmedia.Data.Entities;
using Vmedia.Data.Imp;

namespace Vmedia.Data.Repositories
{
    public class ClientBalancesRepository : RepositoryBase<ClientBalance>, IClientBalancesRepository
    {
        public ClientBalancesRepository(string connectionString) : base(connectionString)
        {

        }

        public IEnumerable<ClientBalance> GetForClient(int clientId)
        {
            IEnumerable<ClientBalance> result = null;
            using (var sql = CreateSqlConnection())
            {
                result = sql.Query<ClientBalance>("SELECT * FROM ClientBalances WHERE ClientId = @clientId",
                    new {clientId});
            }

            return result;
        }

        public ClientBalance Get(int id)
        {
            using (var sql = CreateSqlConnection())
            {
                return sql.QueryFirstOrDefault<ClientBalance>("SELECT * FROM ClientBalances WHERE Id = @id",new { id });
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Vmedia.Data.Entities;
using Vmedia.Data.Imp;

namespace Vmedia.Data.Repositories
{
    public class TransactionsRepository : RepositoryBase<Transaction>, ITransactionsRepository
    {
        public TransactionsRepository(string connectionString) : base(connectionString)
        {

        }

        public IEnumerable<Transaction> GetListForClient(int clientId, int transactionLastId)
        {
            string query = "SELECT t.*,cbs.*,cbt.*,c.*,ct.* FROM AllTransactions t " +
                           "INNER JOIN ClientBalances cbs ON cbs.Id = t.SourceBalanceId " +
                           "INNER JOIN Clients c ON c.Id = cbs.ClientId " +
                           "LEFT JOIN ClientBalances cbt ON cbt.Id = t.TargetBalanceId " +
                           "LEFT JOIN Clients ct ON ct.Id = cbt.ClientId " +
                           "WHERE (c.Id = @clientId OR ct.Id = @clientId) AND t.Id > @transactionLastId " +
                           "ORDER BY t.Id DESC";
            IEnumerable<Transaction> result = null;
            using (var sql = CreateSqlConnection())
            {
                result = sql.Query<Transaction, ClientBalance, ClientBalance, Client, Client, Transaction>(query,
                    (t, cbs, cbt, c, ct) =>
                    {
                        cbs.Client = c;
                        t.SourceBalance = cbs;
                        if (cbt != null)
                        {
                            cbt.Client = ct;
                            t.TargetBalance = cbt;
                        }
                        return t;
                    },
                    new { clientId, transactionLastId });
            }

            return result;
        }

        public void AddTransaction(OperationParams options)
        {
            using (var sql = CreateSqlConnection())
            {
                var command = new SqlCommand("AddTransaction", (SqlConnection)sql);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@params", SqlDbType.Structured));
                command.Parameters["@params"].Value = CreateDataTable(options);

                sql.Open();
                string result = (string)command.ExecuteScalar();
                if (result != "0")
                {
                    throw new ApplicationException(result);
                }
            }
        }

        private static DataTable CreateDataTable(OperationParams options)
        {
            var table = new DataTable();
            table.Columns.Add("SourceBalanceId", typeof(int));
            table.Columns.Add("SourceValue", typeof(decimal));
            table.Columns.Add("OperationId", typeof(int));
            table.Columns.Add("TargetBalanceId", typeof(int));
            table.Columns.Add("TargetValue", typeof(decimal));

            table.Rows.Add(options.SourceBalanceId, options.SourceValue, options.OperationId, options.TargetBalanceId,
                options.TargetValue);

            return table;
        }

        #region Nested classes

        public class OperationParams
        {
            public int SourceBalanceId { get; set; }
            public decimal SourceValue { get; set; }
            public TransactionType OperationId { get; set; }
            public int? TargetBalanceId { get; set; }
            public decimal? TargetValue { get; set; }
        }

        #endregion
    }
}

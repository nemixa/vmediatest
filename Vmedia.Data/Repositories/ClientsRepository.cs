﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Vmedia.Data.Entities;
using Vmedia.Data.Imp;

namespace Vmedia.Data.Repositories
{
    public class ClientsesRepository : RepositoryBase<Client>, IClientsRepository
    {
        public ClientsesRepository(string connectionString) : base(connectionString)
        {

        }

        public Client Get(string login, string password)
        {
            Client client = null;
            using (var sql = CreateSqlConnection())
            {
                client = sql.Query<Client>("SELECT * FROM Clients WHERE Login = @login AND Password = @password", new {login, password}).FirstOrDefault();
            }

            return client;
        }


        public IEnumerable<Client> GetAllClients()
        {
            string query = "SELECT c.*, cb.*  FROM Clients c " +
                           "INNER JOIN ClientBalances cb ON cb.ClientId = c.Id";
            var clients = new Dictionary<int, Client>();
            using (var sql = CreateSqlConnection())
            {
                sql.Query<Client, ClientBalance, Client>(query,
                    (c, cb) =>
                    {
                        if (!clients.ContainsKey(c.Id))
                        {
                            clients.Add(c.Id, c);
                            c.Balances = new List<ClientBalance>();
                        }

                        c = clients[c.Id];
                        c.Balances.Add(cb);
                        return c;
                    });
            }

            return clients.Values;
        }
    }
}
